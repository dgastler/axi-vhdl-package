----------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.types.all;

package AXIRegWidthPkg is

  constant AXI_ADDR_WIDTH   : integer := 40;
  
end package AXIRegWidthPkg;
