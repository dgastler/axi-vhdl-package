library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.axiRegWidthPkg.all;
use work.axiRegPkg.all;
use work.types.all;


entity axiLiteRegBlocking is
  generic (
    READ_TIMEOUT : integer := 16;
    WRITE_TIMEOUT : integer := 16;
    INCLUDE_ILA  : boolean := false
  );
  port (
    clk_axi     : in  std_logic;
    reset_axi_n : in  std_logic;
    readMOSI    : in  AXIreadMOSI;
    readMISO    : out AXIreadMISO;
    writeMOSI   : in  AXIwriteMOSI;
    writeMISO   : out AXIwriteMISO;
    address     : out std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
    rd_data     : in  slv_32_t;
    wr_data     : out slv_32_t;
    write_en    : out std_logic;
    write_ack   : in  std_logic;
    write_err   : in  std_logic;
    read_req    : out std_logic;
    read_ack    : in  std_logic;
    read_err    : in  std_logic);
end entity axiLiteRegBlocking;

architecture behavioral of axiLiteRegBlocking is

  --state machine
  type read_state_t is (SMR_RESET,
                        SMR_IDLE,
                        SMR_REQUEST,
                        SMR_WAIT,
                        SMR_RESPONSE,
                        SMR_SEND,
                        SMR_ERROR);   
  signal read_state : read_state_t;
  signal read_state_val : std_logic_vector(2 downto 0);
  signal read_address : std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
  signal read_state_counter : integer range 1 to READ_TIMEOUT;  
  
  type write_state_t is (SMW_RESET,
                         SMW_IDLE, 
                         SMW_ADDR,
                         SMW_DATA,
                         SMW_RESPONSE,
                         SMW_RESPOND,
                         SMW_ERROR);
  signal write_state : write_state_t;
  signal write_state_val : std_logic_vector(2 downto 0);
  signal write_address : std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
  signal write_failed  : std_logic;
  signal write_state_counter : integer range 1 to READ_TIMEOUT;  
  
  type arbitrator_state_t is (SMA_RESET,
                              SMA_IDLE,
                              SMA_READ,
                              SMA_WRITE,
                              SMA_ERROR);
  signal arbt_state : arbitrator_state_t;
  signal arbt_state_val : std_logic_vector(2 downto 0);

  
  signal localReadMISO   : AXIreadMISO;
  signal localWriteMISO  : AXIwriteMISO;
  
begin  -- architecture behaioral

  read_state_val_proc: process (read_state) is
  begin  -- process read_state_val_proc
    case read_state is
      when SMR_RESET     => read_state_val <= "001";
      when SMR_IDLE      => read_state_val <= "010";
      when SMR_REQUEST   => read_state_val <= "011";
      when SMR_WAIT      => read_state_val <= "100";
      when SMR_RESPONSE  => read_state_val <= "101";
      when SMR_SEND      => read_state_val <= "110";
      when SMR_ERROR     => read_state_val <= "111";               
      when others        => read_state_val <= "000";               
    end case;
  end process read_state_val_proc;
  write_state_val_proc: process (write_state) is
  begin  -- process write_state_val_proc
    case write_state is
      when SMW_RESET   => write_state_val <= "001";
      when SMW_IDLE    => write_state_val <= "010";
      when SMW_ADDR    => write_state_val <= "011";
      when SMW_DATA    => write_state_val <= "100";
      when SMW_RESPOND => write_state_val <= "101";
      when SMW_ERROR   => write_state_val <= "110";
      when others      => write_state_val <= "000";               
    end case;
  end process write_state_val_proc;
  arbt_state_val_proc: process (arbt_state) is
  begin  -- process arbt_state_val_proc
    case arbt_state is
      when SMA_RESET => arbt_state_val <= "001";
      when SMA_IDLE  => arbt_state_val <= "010";
      when SMA_READ  => arbt_state_val <= "011";
      when SMA_WRITE => arbt_state_val <= "100";
      when SMA_ERROR => arbt_state_val <= "101";
      when others    => arbt_state_val <= "000";               
    end case;
  end process arbt_state_val_proc;

    --ILA for debugging
  ----------------------------------
--  axiReg_ila:
--    TCL_CALL:
--      command: IP_CORE_ILA
--      ADV_TRIGGER: true
--      probes:
--        0:
--          TYPE: 0
--          WIDTH: 3
--          MU_CNT: 2
--        1:
--          TYPE: 1
--          WIDTH: 3
--          MU_CNT: 2
--        2:
--          TYPE: 1
--          WIDTH: 3
--          MU_CNT: 2
--        3:
--          TYPE: 1
--          WIDTH: 1
--          MU_CNT: 2
--        4:
--          TYPE: 1
--          WIDTH: 32
--          MU_CNT: 2
--        5:
--          TYPE: 1
--          WIDTH: 32
--          MU_CNT: 2
--        6:
--          TYPE: 1
--          WIDTH: 32
--          MU_CNT: 2
--        7:
--          TYPE: 1
--          WIDTH: 1
--          MU_CNT: 2

  ----------------------------------
  ila_include: if INCLUDE_ILA generate
    ila: entity work.axiReg_ila
      port map (
        clk => clk_axi,
        probe0(2 downto 0) => arbt_state_val,
        probe1(2 downto 0) => write_state_val,
        probe2(2 downto 0) => read_state_val,
        probe3(0)          => reset_axi_n,
        probe4(31 downto 0) => read_address(31 downto 0),
        probe5(31 downto 0) => write_address(31 downto 0),
        probe6(31 downto 0) => std_logic_vector(to_unsigned(read_state_counter,32)),
        probe7(0) => read_ack
        );
  end generate ila_include;


  -------------------------------------------------------------------------------
  -- RW arbitrator
  -------------------------------------------------------------------------------
  arbitrator_state_machine_ctr: process (clk_axi, reset_axi_n) is
  begin  -- process arbitrator_state_machine_ctr
    if reset_axi_n = '0' then          -- asynchronous reset (active high)
      arbt_state <= SMA_RESET;
    elsif clk_axi'event and clk_axi = '1' then  -- rising clock edge
      case arbt_state is
        when SMA_RESET       =>  arbt_state <= SMA_IDLE ;
                                 
        when SMA_IDLE        =>
          if writeMOSI.address_valid = '1' and localWriteMISO.ready_for_address = '1' then
            arbt_state <= SMA_WRITE;
          elsif localReadMISO.ready_for_address = '1' and readMOSI.address_valid = '1' then
            arbt_state <= SMA_READ;
          else
            arbt_state <= SMA_IDLE;
          end if;
        when SMA_READ        =>
          if localReadMISO.data_valid = '1' and readMOSI.ready_for_data = '1' then
            arbt_state <= SMA_IDLE;
          else
            arbt_state <= SMA_READ;
          end if;
        when SMA_WRITE       =>
          if localWriteMISO.response_valid = '1' and writeMOSI.ready_for_response = '1' then
            arbt_state <= SMA_IDLE;
          else
            arbt_state <= SMA_WRITE;
          end if;
        when others          => arbt_state <= SMA_RESET;
      end case;
    end if;
  end process arbitrator_state_machine_ctr; 

  arbitrator_state_machine_proc: process (arbt_state,read_address,write_address) is
  begin  -- process arbitrator_state_machine_proc
    case arbt_state is
      when SMA_READ          => address <= "00" & read_address (AXI_ADDR_WIDTH-1 downto 2);
      when SMA_WRITE         => address <= "00" & write_address(AXI_ADDR_WIDTH-1 downto 2);
      when others            => address <= (others => '1');
    end case;
  end process arbitrator_state_machine_proc;


  -------------------------------------------------------------------------------
  -- Read operations
  -------------------------------------------------------------------------------
  read_state_machine_ctrl: process (clk_axi, reset_axi_n) is
  begin  -- process read_state_machine_ctrl
    if reset_axi_n = '0' then           -- asynchronous reset (active low)
      read_state <= SMR_RESET;
    elsif clk_axi'event and clk_axi = '1' then  -- rising clock edge
      case read_state is
        when SMR_RESET         =>  read_state <= SMR_IDLE;                                   
        when SMR_IDLE          =>
          if localReadMISO.ready_for_address = '1' and readMOSI.address_valid = '1' then
            read_state <= SMR_REQUEST;
          else
            read_state <= SMR_IDLE;
          end if;
        when SMR_REQUEST       =>
          if arbt_state = SMA_READ then
            --We force the read SM to wait until the arbt SM changes state to
            --give priority to the write SM
            read_state <= SMR_WAIT;            
          end if;
        when SMR_WAIT =>
          if read_ack = '1' or read_state_counter = READ_TIMEOUT then
            read_state <= SMR_RESPONSE;
          end if;
        when SMR_RESPONSE      =>
          if readMOSI.ready_for_data = '1' then
            read_state <= SMR_SEND;
          end if;
        when SMR_SEND          =>
          if localReadMISO.data_valid = '1' and readMOSI.ready_for_data = '1' then
            read_state <= SMR_IDLE;
          else
            read_state <= SMR_SEND;
          end if;            

        when others            =>
          read_state <= SMR_RESET;
      end case;
    end if;
  end process read_state_machine_ctrl;

  read_state_machine_latch: process (clk_axi, reset_axi_n) is
  begin  -- process read_state_machine_latch
    if reset_axi_n = '0' then           -- asynchronous reset (active high)
      readMISO  <= DefaultAXIReadMISO;
    elsif clk_axi'event and clk_axi = '1' then  -- rising clock edge
      readMISO.ready_for_address  <= localReadMISO.ready_for_address;
      readMISO.data_valid         <= localReadMISO.data_valid;

      case read_state is
        when SMR_IDLE =>
          readMISO.response <= "00";
          if localReadMISO.ready_for_address = '1' and readMOSI.address_valid = '1' then
            read_address <= readMOSI.address; --latch the address
          end if;
        when SMR_REQUEST =>
          read_state_counter <= 1;
        when SMR_WAIT =>
          --we got the data
          if read_ack = '1' then
            readMISO.data <= rd_data;
            --OK address found
            if read_err = '0' then
              readMISO.response <= RD_RSPN_OKAY;
            else
              readMISO.response <= RD_RSPN_SLVERR;
            end if;
            
          elsif read_state_counter /= READ_TIMEOUT then
            read_state_counter <= read_state_counter + 1;
          elsif read_state_counter = READ_TIMEOUT then
            --timeout
            --Address not found                       
            readMISO.response <= RD_RSPN_DECERR;
          end if;          
        when others => null;
      end case;
    end if;
  end process read_state_machine_latch;

  read_state_machine_proc: process (read_state,
                                    arbt_state,
                                    rd_data,read_ack,
                                    readMOSI.address_valid,     readMOSI.ready_for_data,
                                    localReadMISO.ready_for_address, localReadMISO.data_valid) is
  begin  -- process read_state_machine_proc
    localReadMISO.ready_for_address <= '0';
    localReadMISO.data_valid <= '0';                        
    read_req <= '0';
    
    case read_state is
      when SMR_RESET => NULL;
      when SMR_IDLE  =>
        localReadMISO.ready_for_address <= '1';
      when SMR_REQUEST =>
        if arbt_state = SMA_READ then          
          read_req <= '1';          
        end if;
      when SMR_SEND =>
        localReadMISO.data_valid <= '1';
      when others => null;
    end case;
  end process read_state_machine_proc;

  -------------------------------------------------------------------------------
  -- Write operations
  -------------------------------------------------------------------------------
  write_state_machine_ctrl: process (clk_axi, reset_axi_n) is
  begin  -- process write_state_machine_ctrl
    if reset_axi_n = '0' then           -- asynchronous reset (active low)
      write_state <= SMW_RESET;
      writeMISO  <= DefaultAXIWriteMISO;
    elsif clk_axi'event and clk_axi = '1' then  -- rising clock edge
      writeMISO <= localWriteMISO;
      write_en <= '0';
      
      case write_state is
        when SMW_RESET =>
          write_state <= SMW_IDLE;        
        when SMW_IDLE =>
          if writeMOSI.address_valid = '1' and localWriteMISO.ready_for_address = '1' then          
            write_state <= SMW_ADDR;
            write_address <= writeMOSI.address; --latch the address
          end if;
        when SMW_ADDR =>
          if writeMOSI.data_valid = '1' and localWriteMISO.ready_for_data = '1' then
            write_state <= SMW_DATA;
            wr_data <= writeMOSI.data;
            write_en <= '1';        
          end if;
        when SMW_DATA =>
          write_state <= SMW_RESPONSE;
          write_failed <= '0';
        when SMW_RESPONSE =>
          if write_ack = '1' or write_state_counter = WRITE_TIMEOUT then
            write_failed <= write_err;
            write_state  <= SMW_RESPOND;
          end if;
        when SMW_RESPOND =>
          if localWriteMISO.response_valid = '1' and writeMOSI.ready_for_response = '1' then
            write_state <= SMW_IDLE;          
          end if;
        when others =>    
          write_state <= SMW_RESET;
      end case;
    end if;
  end process write_state_machine_ctrl;
  
  write_state_machine_proc: process (write_state,
                                     arbt_state,
                                     writeMOSI.address_valid, localWriteMISO.ready_for_address
                                     ) is
  begin  -- process write_state_machine_proc
    localWriteMISO <= DefaultAXIWriteMISO;
    case write_state is    
      when SMW_RESET => NULL;
      when SMW_IDLE  =>
        localWriteMISO.ready_for_address <= '1';
        if writeMOSI.address_valid = '1' and localWriteMISO.ready_for_address = '1' then
          
        end if;
      when SMW_ADDR  =>
        if arbt_state = SMA_WRITE then
          localWriteMISO.ready_for_data <= '1';
        end if;
      when SMW_DATA =>
        write_state_counter <= 1;
      when SMW_RESPONSE =>
        write_state_counter <= write_state_counter + 1;
      when SMW_RESPOND =>
        localWriteMISO.response_valid <= '1';
        if write_state_counter = WRITE_TIMEOUT then
          localWriteMISO.response <= WR_RSPN_DECERR;
        elsif write_failed = '0' then
          localWriteMISO.response <= WR_RSPN_OKAY; 
        else
          localWriteMISO.response <= WR_RSPN_SLVERR;
        end if;        
      when others => null;
    end case;
  end process write_state_machine_proc;
end architecture behavioral;

